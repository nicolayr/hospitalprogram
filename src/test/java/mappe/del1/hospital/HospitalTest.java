package mappe.del1.hospital;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HospitalTest {

    // Arrange
    Patient nico = new Patient("Nicolay", "Roness", "123");
    Surgeon nora = new Surgeon("Nora", "Bilet", "999");
    Nurse george = new Nurse("George", "Nasher", "598");
    Department medicine = new Department("Medicine");

    @Test
    @DisplayName("Testing tests")
    public void test() {
        assertTrue(3>2);
    }

    @Nested
    class doesToStringWork {

        @Test
        public void printPerson() {
            System.out.println(nico);
        }
        // This test and the test before were used by me in the process of learning to test.
        // I decided to keep these to give a more complete picture of the process of making the program.
        // I would obviously remove these if this program was intended for a real customer.
    }

    // Act
    @Nested
    class addingToDepartment {

        @Test
        @DisplayName("Add patient to a department")
        public void addPatientToDepartment() {
            medicine.addPatient(nico);
            System.out.println(medicine);
        }

        @Test
        @DisplayName("Adding surgeon to departmant")
        public void addSurgeonToDepartment() {
            medicine.addEmployee(nora);
            System.out.println(medicine);
        }
        // I notice that although my toString()-methods work well, they are not easy to customize.

        @Test
        @DisplayName("Adding a patient that is already in the department")
        public void addDuplicatePerson() {
            medicine.addPatient(nico);
            medicine.addEmployee(nora);
            medicine.addEmployee(nora);
            System.out.println(medicine);
            //We want this test to show the same list as the previous test; in other words
            //it should recognise that these people are already in the list.
        }
    }

    @Nested
    class removePersonFromDepartmentTests {

        @Test
        @DisplayName("Removing person that IS in the list")
        public void removePersonInList() {
            medicine.addPatient(nico);
            medicine.addEmployee(nora);
            try {
                medicine.remove(nico);
            } catch (RemoveException e) {
                System.out.println("The person you entered was not found in this department");
            }
            System.out.println(medicine);
        }

        @Test
        @DisplayName("Removing person that is NOT in the list")
        public void removePersonNotInList() {
            medicine.addPatient(nico);
            try {
                medicine.remove(george);
            } catch (RemoveException e) {
                assertEquals(e.getMessage(), "The employee does not work in this department");
            }
            // Negativ testing
        }

        // Since the remove()-method only accepts "People" objects, i did not see the need to add further
        // input-control. The method simply wouldn't work if anything other than a patient or an employee is added.
        // I could add a check to be safe in case anyone adds different subclasses to "Person" whom are not a patient
        // nor an employee, but i my opinion this does not make sense to do for this program, and
        // since the program is _not_ compromised by not adding it, i decided not to do it.
        // Extra checks for testing should be done in the client-program though, for example one should make sure that
        // if a user were to try and remove a person, there should be added a safety net to make sure that the input
        // is of the object Person.
    }

    // Assert
    // I have checked the results of all tests, and can verify that the methods have worked the way i wanted.
}
