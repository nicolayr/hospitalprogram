package mappe.del1.hospital;

public abstract class Person {

    // Object variables
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Constructor of Person
     * @param firstName first name of person
     * @param lastName last name of person
     * @param socialSecurityNumber ssn for the person
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Getter
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Getter
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Getter
     * @return socialSecurityNumber
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Setter
     * @param firstName firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Setter
     * @param lastName lastName
     */
    public void setLastName(String lastName) {
        this.firstName = firstName;
    }

    /**
     * Setter
     * @param socialSecurityNumber socialSecurityNumber
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Gets full name of person
     * @return full name og person, formatted
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }

    /**
     *
     * @return all needed information of Person; full name first, with the social security number underneath.
     */
    @Override
    public String toString() {
        return "Name: " + getFullName() + "\nSocial Security Number: " + socialSecurityNumber;
    }


}
