/**
 * An object that represents one of the professions at the hospital
 */
package mappe.del1.hospital;

public class Nurse extends Employee{

    /**
     * Constructor
     * @param firstName first name
     * @param lastName last name
     * @param socialSecurityNumber social security number
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString() method
     * @return desired format for printing nurses
     */
    public String toString() {
        return "Nurse\n" + super.toString();
    }

}
