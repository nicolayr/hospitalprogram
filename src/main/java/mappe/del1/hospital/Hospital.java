/**
 * An object representing a hospital and its departments
 */
package mappe.del1.hospital;

import java.util.ArrayList;

public class Hospital {

    // Object-variables
    private final String hospitalName;
    private ArrayList<Department> departments = new ArrayList<>();

    /**
     * Constructor
     * @param hospitalName name of hospital
     */
    public Hospital (String hospitalName) {
        this.hospitalName = hospitalName;
    }

    /**
     * Getter
     * @return name of hospital
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Getter
     * @return list of department-objects registered to the hospital
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Adds a department to the hospital, if the department is not already there.
     * @param department
     */
    public void addDepartment(Department department) {
        if(!departments.contains(department)) {
            departments.add(department);
        }
    }

    /**
     * toString() method
     * @return full overview of department in the desired format
     */
    public String toString() {
        String departmentList = "";
        for (Department d: departments) {
            departmentList += d.getDepartmentName() + "\n";
        }
        return "-----Departments-----\n" + departmentList;
    }
}
