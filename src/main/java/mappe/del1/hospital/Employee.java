/**
 * A class to be inherited by all the different employees in the hospital.
 */
package mappe.del1.hospital;

public class Employee extends Person{

    /**
     * Constructor
     *
     * @param firstName first name
     * @param lastName last name
     * @param socialSecurityNumber social security number
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Prints employee at the desired format
     * @return Employee: + toString() inherited from Person
     */
    public String toString() {
        return super.toString() + "\n";
    }
}
