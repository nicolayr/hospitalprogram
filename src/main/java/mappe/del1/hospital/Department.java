/**
 * A class for creating departments for hospitals.
 */
package mappe.del1.hospital;

import java.util.ArrayList;
import java.util.Objects;

public class Department {

    // Object-variables
    private String departmentName;
    private ArrayList<Employee> employees = new ArrayList<>();
    private ArrayList<Patient> patients = new ArrayList<>();
    // These two were added an top of what was given in the class-diagram.

    /**
     * Constructor
     * @param departmentName name of the department
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Getter
     * @return name of department
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Setter
     * @param departmentName new name of department
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Getter
     * @return a list of employees in the department
     */
    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * Adds new employee to a department, if the employee is not already there
     * @param employee employee
     */
    public void addEmployee(Employee employee) {
        if(!employees.contains(employee)) {
            employees.add(employee);
        }
    }

    /**
     * Getter
     * @return a list of patients in the department
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Adds new patient to a department, if the employee is not already there
     * @param patient patient
     */
    public void addPatient(Patient patient) {
        if(!patients.contains(patient)) {
        patients.add(patient);
        }
    }

    /**
     * Removes person from the department. Checks whether the person is a patient or an employee, and tries to
     * remove them from the corresponding list. If the person is not found in the list, a RemoveException is thrown.
     * @param person person, either a patient or an employee, that you wish to remove.
     * @throws RemoveException a checked exception to be thrown when the person is not found in either list.
     */
    public void remove(Person person) throws RemoveException{
        if(person instanceof Employee) {
            if(employees.contains(person)) {
                employees.remove(person);
            }
            else {
                throw new RemoveException("The employee does not work in this department");
            }
        }
        else if(person instanceof Patient) {
            if(patients.contains(person)) {
                patients.remove(person);
            }
            else {
                throw new RemoveException("The patient is not in this department");
            }
        }
        else {
            throw new RemoveException("The person you enter must be an employee or a patient");
        }
    }

    /**
     * Checks if the given object is the same as the object in which the method is applied to.
     * @param o object to check
     * @return true if the object is the same, false if not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) && Objects.equals(employees, that.employees) && Objects.equals(patients, that.patients);
    }
    // This method was generated with IntelliJ


    /**
     * Gives a value for an object. Two objects that are determined equal by the equals()-method must have
     * the same hashCode-value.
     * @return hashCode-value.
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }
    // This method was generated with IntelliJ


    /**
     * toString()-method
     * @return all employees and patients in their desired format, as well as the name of the department they're in
     */
    @Override
    public String toString() {
        String patientsList = "";
        String employeeList = "";
        for (Patient p : patients) {
            patientsList += p.toString() + "\n";
        }
        for (Employee e: employees) {
            employeeList += e.toString() + "\n";
        }
        return "-----" + departmentName + "------\n" +
                "\nEmployees:\n" + employeeList +
                "\nPatients: \n" + patientsList;
    }


}