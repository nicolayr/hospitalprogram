/**
 * A class to make a custom, throwable exception used for handling removing of objects
 */
package mappe.del1.hospital;
import java.io.Serial;

public class RemoveException extends Exception{

    /**
     * Defining the serialVersionUID for this class
     */
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * The actual Exception-method that will be used in other classes.
     * @param errorMessage message to be displayed when an error occurs.
     */
    public RemoveException(String errorMessage){
        super(errorMessage);
    }

}
