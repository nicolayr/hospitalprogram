package mappe.del1.hospital;

public interface Diagnosable {

    public abstract void setDiagnosis(String diagnosis);
}
