/**
 * Object to represent one of the professions at the hospital.
 * The general Practitioner _can_ set diagnoses on patients.
 */
package mappe.del1.hospital;

public class GeneralPractitioner extends Doctor{

    /**
     * Constructor
     * @param firstName first name
     * @param lastName last name
     * @param socialSecurityNumber social security number
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a diagnosis (as a String) on a particular patient.
     * @param patient the patient in which a doctor wishes to diagnose. Is of type
     *                patient, because patient are the only people in the hospital
     *                that can be diagnosed.
     * @param diagnosis the diagnose given by the doctor.
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

    /**
     * toString() method
     * @return desired format for printing General Practitioners
     */
    public String toString() {
        return "General Practitioner\n" + super.toString();
    }
}
