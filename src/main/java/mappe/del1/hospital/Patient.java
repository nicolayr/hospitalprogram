/**
 * A class used to create patient objects. Patient can be diagnosed.
 */
package mappe.del1.hospital;

public class Patient extends Person implements Diagnosable{
    private String diagnosis = "Not yet diagnosed";

    /**
     * Constructor - it is protected as by request by the given class diagram.
     * @param firstName first name
     * @param lastName last name
     * @param socialSecurityNumber social security number
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName,lastName, socialSecurityNumber);
    }

    /**
     * Getter
     * @return diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Setter
     * @param diagnosis diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * toString() method
     * @return desired format for printing patients.
     */
    public String toString() {
        return "Patient\n" + super.toString() + "\nDiagnosis: " + diagnosis + "\n";
    }
}
