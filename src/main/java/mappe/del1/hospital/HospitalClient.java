package mappe.del1.hospital;

public class HospitalClient {
    public static void main(String[] args) {

        // 5.1: Creating the hospital and adding the test data.
        Hospital kalnes = new Hospital("Kalnes");
        HospitalTestData.fillRegisterWithTestData(kalnes);

        // Test-patient
        Patient nico = new Patient("Nicolay", "Roness", "123");


        System.out.println(kalnes.getDepartments());

        // 5.2: removing an employee from a department
        try {
            kalnes.getDepartments().get(0).remove(kalnes.getDepartments().get(0).getEmployees().get(0));
        } catch (RemoveException e) {
            System.out.println("Something went wrong");
        }

        System.out.println(kalnes.getDepartments());

        // 5.3: removing a patient not in the department
        try {
            kalnes.getDepartments().get(0).remove(nico);
            System.out.println("Nico was removed");
        } catch (RemoveException e) {
            System.out.println(nico.getFullName() + " is not this department");
        }

    }
}
