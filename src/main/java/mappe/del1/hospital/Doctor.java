/**
 * A class to be extended by different types of doctors. Class is abstract as it does not make sense to
 * create a Doctor-object.
 */

package mappe.del1.hospital;

public abstract class Doctor extends Employee {

    /**
     * Doctor-constructor. Is "protected" as per request by the given class diagram.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString() method
     * @return data needed when printing an object of the class.
     */
    public String toString() {
        return super.toString();
    }

    /**
     * Sets a diagnosis (as a String) on a particular patient.
     * @param patient the patient in which a doctor wishes to diagnose. Is of type
     *                patient, because patient are the only people in the hospital
     *                that can be diagnosed.
     * @param diagnosis the diagnose given by the doctor.
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
